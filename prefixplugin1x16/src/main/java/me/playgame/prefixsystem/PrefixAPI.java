package me.playgame.prefixsystem;

import me.playgame.prefixsystem.handler.PrefixHandler;
import me.playgame.prefixsystem.instances.Format;
import org.bukkit.entity.Player;

public class PrefixAPI {

    public void setChatPrefix(Player player, String prefix){
        PrefixHandler.setPlayersChatPrefix(player, prefix);
    }

    public void setChatSuffix(Player player, String suffix){
        PrefixHandler.setPlayersChatSuffix(player, suffix);
    }

    public void setTablistPrefix(Player player, String prefix){
        PrefixHandler.setPlayersTablistPrefix(player, prefix);
    }

    public void setTablistSuffix(Player player, String suffix){
        PrefixHandler.setPlayersTablistSuffix(player, suffix);
    }

    public void setTablistWeight(Player player, int weight){
        PrefixHandler.setPlayersTablistWeight(player, String.valueOf(weight));
    }

    public void setDisplayColor(Player player, String displaycolor){
        PrefixHandler.setPlayersDisplaycolor(player, displaycolor);
    }

    public void reloadAppearance(Player player){
        PrefixHandler.reloadAppearance(player);
    }

    public void setAppearance(Player player, Format format){
        PrefixHandler.setAppearance(player, format);
    }

}
