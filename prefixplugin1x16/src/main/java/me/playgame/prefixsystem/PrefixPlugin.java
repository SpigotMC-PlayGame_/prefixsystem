package me.playgame.prefixsystem;

import lombok.Getter;
import me.playgame.prefixsystem.commands.PrefixCommand;
import me.playgame.prefixsystem.handler.PrefixHandler;
import me.playgame.prefixsystem.handler.TablistHandler;
import me.playgame.prefixsystem.listener.AsyncPlayerChatListener;
import me.playgame.prefixsystem.listener.PlayerJoinListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

@Getter
public class PrefixPlugin extends JavaPlugin {

    private TablistHandler tablistHandler;
    private PrefixHandler prefixHandler;

    @Override
    public void onEnable() {

        // Creating default config if not exists
        if(!getDataFolder().exists()){
            getDataFolder().mkdir();
        }
        if(!new File(getDataFolder(), "config.yml").exists()){
            saveResource("config.yml", true);
        }

        // Loading Manager...
        this.tablistHandler = new TablistHandler(this);
        this.prefixHandler = new PrefixHandler(this);

        // Loading listener...
        new PlayerJoinListener(this);
        new AsyncPlayerChatListener(this);

        // Loading commands...
        new PrefixCommand(this);

        getLogger().info("Prefixsystem wird geladen...");
        getLogger().info(" ");
        getLogger().info("Author: PlayGame_");
        getLogger().info("Version: " + getDescription().getVersion());
        getLogger().info("Needed Serverversion: " + getDescription().getAPIVersion());
        getLogger().info("PlaceholderAPI installed: " + (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null));
        getLogger().info(" ");
        getLogger().info("Prefixsystem wurde geladen!");
        super.onEnable();
    }
}

