package me.playgame.prefixsystem.commands;

import me.playgame.prefixsystem.PrefixPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrefixCommand implements CommandExecutor {

    private final PrefixPlugin plugin;
    private final Map<String, String> messages = new HashMap<>();
    private final List<String> messageKeys = new ArrayList<>();

    public PrefixCommand(PrefixPlugin plugin) {
        this.plugin = plugin;
        Bukkit.getScheduler().runTaskAsynchronously(plugin, ()->{
            messageKeys.add("prefix-command-syntax-error");
            messageKeys.add("prefix-command-help-title");
            messageKeys.add("prefix-command-help");
            messageKeys.add("prefix-command-help-reload");
            messageKeys.add("prefix-command-help-reload-player");
            messageKeys.add("prefix-command-prefix-reloaded");
            messageKeys.add("prefix-command-prefix-reloaded-player");
            messageKeys.add("prefix-command-prefix-player-not-online");

            if(plugin.getConfig().contains("messages")){
                messageKeys.forEach(messageKey ->{
                    if(plugin.getConfig().contains("messages." + messageKey)){
                        messages.put(messageKey, ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("messages." + messageKey)));
                    }else{
                        plugin.getLogger().warning("Die Nachricht '" + messageKey + "' fehlt.");
                    }
                });
            }else{
                plugin.getLogger().warning("Es wurden keine Nachrichten in der Config gefunden. Bitte nutze die aktuelle Version der Config.");
            }
        });

        plugin.getCommand("prefix").setExecutor(this);
    }

    @Override
    public boolean onCommand(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        if(args.length == 0){
            commandSender.sendMessage(messages.get("prefix-command-syntax-error"));
        }else if(args.length == 1){
            if(args[0].equalsIgnoreCase("help")){
                commandSender.sendMessage(messages.get("prefix-command-help-title"));
                commandSender.sendMessage(messages.get("prefix-command-help"));
                commandSender.sendMessage(messages.get("prefix-command-help-reload"));
                commandSender.sendMessage(messages.get("prefix-command-help-reload-player"));
            }else if(args[0].equalsIgnoreCase("reload")){
                plugin.reloadConfig();
                Bukkit.getScheduler().runTaskAsynchronously(plugin, ()->{
                    Bukkit.getOnlinePlayers().forEach(online ->{
                        plugin.getPrefixHandler().setPlayerAppearanceAutomatically(online);
                    });
                    commandSender.sendMessage(messages.get("prefix-command-prefix-reloaded"));
                });
            }else{
                commandSender.sendMessage(messages.get("prefix-command-syntax-error"));
            }
        }else if(args.length == 2){
            if(args[0].equalsIgnoreCase("reload")){
                plugin.reloadConfig();
                final Player target = Bukkit.getPlayer(args[1]);
                if(target != null){
                    plugin.getPrefixHandler().setPlayerAppearanceAutomatically(target);
                    commandSender.sendMessage(messages.get("prefix-command-prefix-reloaded-player").replace("%player%", target.getName()));
                }else{
                    commandSender.sendMessage(messages.get("prefix-command-prefix-player-not-online"));
                }
            }else{
                commandSender.sendMessage(messages.get("prefix-command-syntax-error"));
            }
        }else{
            commandSender.sendMessage(messages.get("prefix-command-syntax-error"));
        }
        return true;
    }
}
