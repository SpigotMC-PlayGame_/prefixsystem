package me.playgame.prefixsystem.handler;

import lombok.Getter;
import me.clip.placeholderapi.PlaceholderAPI;
import me.playgame.prefixsystem.PrefixPlugin;
import me.playgame.prefixsystem.instances.Format;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class PrefixHandler {

    private static PrefixPlugin plugin;

    @Getter
    private static final HashMap<UUID, Format> playersChatFormat = new HashMap<>();
    private static final HashMap<String, Format> availableFormats = new HashMap<>();
    private static final List<Format> defaultFormat = new ArrayList<>();

    public PrefixHandler(PrefixPlugin plugin) {
        PrefixHandler.plugin = plugin;
        Bukkit.getScheduler().runTaskAsynchronously(plugin, ()->{
            plugin.getConfig().getStringList("groups").forEach(group ->{
                String tablistPrefix;
                String tablistSuffix;
                String chatPrefix;
                String chatSuffix;
                String displaycolor;
                String tablistWeight;
                String permission;
                boolean defaultGroup = false;
                plugin.getLogger().info("Die Gruppe '" + group + "' wird geladen...");

                // Tablist start
                if(plugin.getConfig().contains(group + ".tablistPrefix")){
                    tablistPrefix = plugin.getConfig().getString(group + ".tablistPrefix");
                    plugin.getLogger().info(group + " : TablistPrefix : OK");
                }else{
                    plugin.getLogger().info(group + " : TablistPrefix : NOTFOUND");
                    plugin.getLogger().info("ERROR: Die Gruppe '" + group + "' konnte nicht geladen werden.");
                    return;
                }
                if(plugin.getConfig().contains(group + ".tablistSuffix")){
                    tablistSuffix = plugin.getConfig().getString(group + ".tablistSuffix");
                    plugin.getLogger().info(group + " : TablistSuffix : OK");
                }else{
                    plugin.getLogger().info(group + " : TablistSuffix : NOTFOUND");
                    plugin.getLogger().info("ERROR: Die Gruppe '" + group + "' konnte nicht geladen werden.");
                    return;
                }
                if(plugin.getConfig().contains(group + ".tablistWeight")){
                    tablistWeight = plugin.getConfig().getString(group + ".tablistWeight");
                    plugin.getLogger().info(group + " : TablistWeight : OK");
                }else{
                    plugin.getLogger().info(group + " : TablistWeight : NOTFOUND");
                    plugin.getLogger().info("ERROR: Die Gruppe '" + group + "' konnte nicht geladen werden.");
                    return;
                }
                //Tablist end

                //Chat start
                if(plugin.getConfig().contains(group + ".chatPrefix")){
                    chatPrefix = plugin.getConfig().getString(group + ".chatPrefix");
                    plugin.getLogger().info(group + " : ChatPrefix : OK");
                }else{
                    plugin.getLogger().info(group + " : ChatPrefix : NOTFOUND");
                    plugin.getLogger().info("ERROR: Die Gruppe '" + group + "' konnte nicht geladen werden.");
                    return;
                }
                if(plugin.getConfig().contains(group + ".chatSuffix")){
                    chatSuffix = plugin.getConfig().getString(group + ".chatSuffix");
                    plugin.getLogger().info(group + " : ChatSuffix : OK");
                }else{
                    plugin.getLogger().info(group + " : ChatSuffix : NOTFOUND");
                    plugin.getLogger().info("ERROR: Die Gruppe '" + group + "' konnte nicht geladen werden.");
                    return;
                }
                //Chat end

                if(plugin.getConfig().contains(group + ".displaycolor")){
                    displaycolor = plugin.getConfig().getString(group + ".displaycolor");
                    plugin.getLogger().info(group + " : Displaycolor : OK");
                }else{
                    plugin.getLogger().info(group + " : Displaycolor : NOTFOUND");
                    plugin.getLogger().info("ERROR: Die Gruppe '" + group + "' konnte nicht geladen werden.");
                    return;
                }

                if(plugin.getConfig().contains(group + ".permission")){
                    permission = plugin.getConfig().getString(group + ".permission");
                    plugin.getLogger().info(group + " : Permission : OK");
                }else{
                    plugin.getLogger().info(group + " : Permission : NOTFOUND");
                    plugin.getLogger().info("ERROR: Die Gruppe '" + group + "' konnte nicht geladen werden.");
                    return;
                }
                if(plugin.getConfig().contains(group + ".default")){
                    defaultGroup = plugin.getConfig().getBoolean(group + ".default");
                }
                if(defaultGroup){
                    defaultFormat.add(0, new Format(tablistPrefix, tablistSuffix, tablistWeight, chatPrefix, chatSuffix, displaycolor, permission));
                }else{
                    availableFormats.put(permission, new Format(tablistPrefix, tablistSuffix, tablistWeight, chatPrefix, chatSuffix, displaycolor, permission));
                }
                plugin.getLogger().info("Die Gruppe '" + group + "' wurde erfolgreich geladen.");
            });
            if(defaultFormat.get(0) == null){
                plugin.getLogger().info("ERROR: Es wurde keine 'default' Gruppe festgelegt das kann zu fehlern führen.");
            }
        });
    }

    public void setPlayerAppearanceAutomatically(final Player player){
        Format format = null;
        final boolean placeholderapi = Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI");
        for (String permission : availableFormats.keySet()){
            if(player.hasPermission(permission)){
                format = availableFormats.get(permission);
            }
        }

        if(format == null){
            if(defaultFormat.get(0)!=null){
                format = defaultFormat.get(0);
            }else{
                plugin.getLogger().info("ERROR: Es konnte kein Prefix bei " + player.getName() + " gesetzt werden da keine 'default' Gruppe existiert.");
                return;
            }
        }

        String tablistPrefix = ChatColor.translateAlternateColorCodes('&', placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistPrefix()):format.getTablistPrefix());
        String tablistSuffix = ChatColor.translateAlternateColorCodes('&', placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistSuffix()):format.getTablistSuffix());
        String tablistWeight = placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistWeight()):format.getTablistWeight();

        plugin.getTablistHandler().registerTeam(player, tablistPrefix, tablistSuffix, format.getDisplaycolor(), tablistWeight);
        player.setDisplayName(ChatColor.translateAlternateColorCodes('&', format.getDisplaycolor()) + player.getName());

        playersChatFormat.put(player.getUniqueId(), format);
    }

    public static void setPlayersTablistPrefix(final Player player, String prefix){
        Format format = new Format("&f", "&f", "99", "&f", "&f", "&f", "none");
        if(playersChatFormat.containsKey(player.getUniqueId())){
            format = playersChatFormat.get(player.getUniqueId());
        }
        format.setTablistPrefix(prefix);
        final boolean placeholderapi = Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI");

        String tablistPrefix = ChatColor.translateAlternateColorCodes('&', placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistPrefix()):format.getTablistPrefix());
        String tablistSuffix = ChatColor.translateAlternateColorCodes('&', placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistSuffix()):format.getTablistSuffix());
        String tablistWeight = placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistWeight()):format.getTablistWeight();

        plugin.getTablistHandler().registerTeam(player, tablistPrefix, tablistSuffix, format.getDisplaycolor(), tablistWeight);
        player.setDisplayName(ChatColor.translateAlternateColorCodes('&', format.getDisplaycolor()) + player.getName());

        playersChatFormat.put(player.getUniqueId(), format);
    }

    public static void setPlayersTablistSuffix(final Player player, String suffix){
        Format format = new Format("&f", "&f", "99", "&f", "&f", "&f", "none");
        if(playersChatFormat.containsKey(player.getUniqueId())){
            format = playersChatFormat.get(player.getUniqueId());
        }
        format.setTablistSuffix(suffix);
        final boolean placeholderapi = Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI");

        String tablistPrefix = ChatColor.translateAlternateColorCodes('&', placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistPrefix()):format.getTablistPrefix());
        String tablistSuffix = ChatColor.translateAlternateColorCodes('&', placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistSuffix()):format.getTablistSuffix());
        String tablistWeight = placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistWeight()):format.getTablistWeight();

        plugin.getTablistHandler().registerTeam(player, tablistPrefix, tablistSuffix, format.getDisplaycolor(), tablistWeight);
        player.setDisplayName(ChatColor.translateAlternateColorCodes('&', format.getDisplaycolor()) + player.getName());

        playersChatFormat.put(player.getUniqueId(), format);
    }

    public static void setPlayersTablistWeight(final Player player, String weight){
        Format format = new Format("&f", "&f", "99", "&f", "&f", "&f", "none");
        if(playersChatFormat.containsKey(player.getUniqueId())){
            format = playersChatFormat.get(player.getUniqueId());
        }
        format.setTablistWeight(weight);
        final boolean placeholderapi = Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI");

        String tablistPrefix = ChatColor.translateAlternateColorCodes('&', placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistPrefix()):format.getTablistPrefix());
        String tablistSuffix = ChatColor.translateAlternateColorCodes('&', placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistSuffix()):format.getTablistSuffix());
        String tablistWeight = placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistWeight()):format.getTablistWeight();

        plugin.getTablistHandler().registerTeam(player, tablistPrefix, tablistSuffix, format.getDisplaycolor(), tablistWeight);
        player.setDisplayName(ChatColor.translateAlternateColorCodes('&', format.getDisplaycolor()) + player.getName());

        playersChatFormat.put(player.getUniqueId(), format);
    }

    public static void setPlayersChatPrefix(final Player player, String prefix){
        Format format = new Format("&f", "&f", "99", "&f", "&f", "&f", "none");
        if(playersChatFormat.containsKey(player.getUniqueId())){
            format = playersChatFormat.get(player.getUniqueId());
        }
        format.setChatPrefix(prefix);
        final boolean placeholderapi = Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI");

        String tablistPrefix = ChatColor.translateAlternateColorCodes('&', placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistPrefix()):format.getTablistPrefix());
        String tablistSuffix = ChatColor.translateAlternateColorCodes('&', placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistSuffix()):format.getTablistSuffix());
        String tablistWeight = placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistWeight()):format.getTablistWeight();

        plugin.getTablistHandler().registerTeam(player, tablistPrefix, tablistSuffix, format.getDisplaycolor(), tablistWeight);
        player.setDisplayName(ChatColor.translateAlternateColorCodes('&', format.getDisplaycolor()) + player.getName());

        playersChatFormat.put(player.getUniqueId(), format);
    }

    public static void setPlayersChatSuffix(final Player player, String suffix){
        Format format = new Format("&f", "&f", "99", "&f", "&f", "&f", "none");
        if(playersChatFormat.containsKey(player.getUniqueId())){
            format = playersChatFormat.get(player.getUniqueId());
        }
        format.setChatSuffix(suffix);
        final boolean placeholderapi = Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI");

        String tablistPrefix = ChatColor.translateAlternateColorCodes('&', placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistPrefix()):format.getTablistPrefix());
        String tablistSuffix = ChatColor.translateAlternateColorCodes('&', placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistSuffix()):format.getTablistSuffix());
        String tablistWeight = placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistWeight()):format.getTablistWeight();

        plugin.getTablistHandler().registerTeam(player, tablistPrefix, tablistSuffix, format.getDisplaycolor(), tablistWeight);
        player.setDisplayName(ChatColor.translateAlternateColorCodes('&', format.getDisplaycolor()) + player.getName());

        playersChatFormat.put(player.getUniqueId(), format);
    }

    public static void setPlayersDisplaycolor(final Player player, String color){
        Format format = new Format("&f", "&f", "99", "&f", "&f", "&f", "none");
        if(playersChatFormat.containsKey(player.getUniqueId())){
            format = playersChatFormat.get(player.getUniqueId());
        }
        format.setDisplaycolor(color);
        final boolean placeholderapi = Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI");

        String tablistPrefix = ChatColor.translateAlternateColorCodes('&', placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistPrefix()):format.getTablistPrefix());
        String tablistSuffix = ChatColor.translateAlternateColorCodes('&', placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistSuffix()):format.getTablistSuffix());
        String tablistWeight = placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistWeight()):format.getTablistWeight();

        plugin.getTablistHandler().registerTeam(player, tablistPrefix, tablistSuffix, format.getDisplaycolor(), tablistWeight);
        player.setDisplayName(ChatColor.translateAlternateColorCodes('&', format.getDisplaycolor()) + player.getName());

        playersChatFormat.put(player.getUniqueId(), format);
    }

    public static void reloadAppearance(Player player){
        Format format = new Format("&f", "&f", "99", "&f", "&f", "&f", "none");
        if(playersChatFormat.containsKey(player.getUniqueId())){
            format = playersChatFormat.get(player.getUniqueId());
        }
        final boolean placeholderapi = Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI");

        String tablistPrefix = ChatColor.translateAlternateColorCodes('&', placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistPrefix()):format.getTablistPrefix());
        String tablistSuffix = ChatColor.translateAlternateColorCodes('&', placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistSuffix()):format.getTablistSuffix());
        String tablistWeight = placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistWeight()):format.getTablistWeight();

        plugin.getTablistHandler().registerTeam(player, tablistPrefix, tablistSuffix, format.getDisplaycolor(), tablistWeight);
        player.setDisplayName(ChatColor.translateAlternateColorCodes('&', format.getDisplaycolor()) + player.getName());

        playersChatFormat.put(player.getUniqueId(), format);
    }

    public static void setAppearance(Player player, Format format){
        final boolean placeholderapi = Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI");

        String tablistPrefix = ChatColor.translateAlternateColorCodes('&', placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistPrefix()):format.getTablistPrefix());
        String tablistSuffix = ChatColor.translateAlternateColorCodes('&', placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistSuffix()):format.getTablistSuffix());
        String tablistWeight = placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getTablistWeight()):format.getTablistWeight();

        plugin.getTablistHandler().registerTeam(player, tablistPrefix, tablistSuffix, format.getDisplaycolor(), tablistWeight);
        player.setDisplayName(ChatColor.translateAlternateColorCodes('&', format.getDisplaycolor()) + player.getName());

        playersChatFormat.put(player.getUniqueId(), format);
    }


}

