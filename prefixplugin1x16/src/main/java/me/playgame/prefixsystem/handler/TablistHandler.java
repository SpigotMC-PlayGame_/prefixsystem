package me.playgame.prefixsystem.handler;

import me.playgame.prefixsystem.PrefixPlugin;
import net.minecraft.server.v1_16_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_16_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class TablistHandler {

    private final Scoreboard scoreboard;
    private final HashMap<UUID, String> teams = new HashMap<>();

    private final PrefixPlugin plugin;

    public TablistHandler(final PrefixPlugin plugin) {
        this.plugin = plugin;
        this.scoreboard = new Scoreboard();
    }

    public void registerTeam(Player player, String prefix, String suffix, String displaycolor, String level){
        String teamname = level + player.getUniqueId().toString().substring(1, 6);

        if(scoreboard.getTeam(teamname) != null){
            scoreboard.removeTeam(scoreboard.getTeam(teamname));
        }
        ScoreboardTeam team = scoreboard.createTeam(teamname);
        team.setColor(translateTabcolor(displaycolor));
        team.setPrefix(new ChatComponentText(prefix));
        team.setSuffix(new ChatComponentText(suffix));

        teams.put(player.getUniqueId(), teamname);
        update();
    }

    public EnumChatFormat translateTabcolor(String color){
        if(color.equalsIgnoreCase("&0")){
            return EnumChatFormat.BLACK;
        }else if(color.equalsIgnoreCase("&1")){
            return EnumChatFormat.DARK_BLUE;
        }else if(color.equalsIgnoreCase("&2")){
            return EnumChatFormat.DARK_GREEN;
        }else if(color.equalsIgnoreCase("&3")){
            return EnumChatFormat.DARK_AQUA;
        }else if(color.equalsIgnoreCase("&4")){
            return EnumChatFormat.DARK_RED;
        }else if(color.equalsIgnoreCase("&5")){
            return EnumChatFormat.DARK_PURPLE;
        }else if(color.equalsIgnoreCase("&6")){
            return EnumChatFormat.GOLD;
        }else if(color.equalsIgnoreCase("&7")){
            return EnumChatFormat.GRAY;
        }else if(color.equalsIgnoreCase("&8")){
            return EnumChatFormat.DARK_GRAY;
        }else if(color.equalsIgnoreCase("&9")){
            return EnumChatFormat.BLUE;
        }else if(color.equalsIgnoreCase("&a")){
            return EnumChatFormat.GREEN;
        }else if(color.equalsIgnoreCase("&b")){
            return EnumChatFormat.AQUA;
        }else if(color.equalsIgnoreCase("&c")){
            return EnumChatFormat.RED;
        }else if(color.equalsIgnoreCase("&d")){
            return EnumChatFormat.LIGHT_PURPLE;
        }else if(color.equalsIgnoreCase("&e")){
            return EnumChatFormat.YELLOW;
        }else if(color.equalsIgnoreCase("&f")){
            return EnumChatFormat.WHITE;
        }else if(color.equalsIgnoreCase("&k")){
            return EnumChatFormat.OBFUSCATED;
        }else if(color.equalsIgnoreCase("&m")){
            return EnumChatFormat.STRIKETHROUGH;
        }else if(color.equalsIgnoreCase("&n")){
            return EnumChatFormat.UNDERLINE;
        }else if(color.equalsIgnoreCase("&l")){
            return EnumChatFormat.BOLD;
        }else if(color.equalsIgnoreCase("&o")){
            return EnumChatFormat.ITALIC;
        }else if(color.equalsIgnoreCase("&r")){
            return EnumChatFormat.RESET;
        }else{
            return EnumChatFormat.WHITE;
        }
    }

    private void update() {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, ()->{
            for (Player online : Bukkit.getOnlinePlayers()) {
                if (!scoreboard.getTeam(teams.get(online.getUniqueId())).getPlayerNameSet().contains(online.getName())) {
                    scoreboard.getTeam(teams.get(online.getUniqueId())).getPlayerNameSet().add(online.getName());
                }
                sendPacket(new PacketPlayOutScoreboardTeam(scoreboard.getTeam(teams.get(online.getUniqueId())), 1));
                sendPacket(new PacketPlayOutScoreboardTeam(scoreboard.getTeam(teams.get(online.getUniqueId())), 0));
            }
        });
    }

    private void sendPacket(Packet<?> packet){
        for (Player online : Bukkit.getOnlinePlayers()) {
            final CraftPlayer craftPlayer = (CraftPlayer) online;
            craftPlayer.getHandle().playerConnection.sendPacket(packet);
        }
    }
}

