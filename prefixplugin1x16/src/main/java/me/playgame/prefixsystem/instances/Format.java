package me.playgame.prefixsystem.instances;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Format {

    private String tablistPrefix;
    private String tablistSuffix;
    private String tablistWeight;
    private String chatPrefix;
    private String chatSuffix;
    private String displaycolor;
    private String permission;

    public Format(String tablistPrefix, String tablistSuffix, String tablistWeight, String chatPrefix, String chatSuffix, String displaycolor, String permission) {
        this.tablistPrefix = tablistPrefix;
        this.tablistSuffix = tablistSuffix;
        this.tablistWeight = tablistWeight;
        this.chatPrefix = chatPrefix;
        this.chatSuffix = chatSuffix;
        this.displaycolor = displaycolor;
        this.permission = permission;
    }

}
