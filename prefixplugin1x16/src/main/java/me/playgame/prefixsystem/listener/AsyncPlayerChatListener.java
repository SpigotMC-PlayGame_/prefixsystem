package me.playgame.prefixsystem.listener;

import me.clip.placeholderapi.PlaceholderAPI;
import me.playgame.prefixsystem.PrefixPlugin;
import me.playgame.prefixsystem.handler.PrefixHandler;
import me.playgame.prefixsystem.instances.Format;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class AsyncPlayerChatListener implements Listener {

    private final PrefixPlugin plugin;

    public AsyncPlayerChatListener(PrefixPlugin plugin) {
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void handleAsyncPlayerChatEvent(AsyncPlayerChatEvent event){
        final Player player = event.getPlayer();
        if(PrefixHandler.getPlayersChatFormat().containsKey(player.getUniqueId())){
            final Format format = PrefixHandler.getPlayersChatFormat().get(player.getUniqueId());
            final boolean placeholderapi = Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI");

            String chatPrefix = ChatColor.translateAlternateColorCodes('&', placeholderapi? PlaceholderAPI.setPlaceholders(player, format.getChatPrefix()):format.getChatPrefix());
            String chatSuffix = ChatColor.translateAlternateColorCodes('&', placeholderapi?PlaceholderAPI.setPlaceholders(player, format.getChatSuffix()):format.getChatSuffix());

            event.setFormat(chatPrefix + player.getDisplayName() + chatSuffix + event.getMessage());
        }else{
            plugin.getLogger().info("[Prefix] ERROR: Es konnte kein Prefix bei " + player.getName() + " gesetzt werden da keine 'default' Gruppe existiert.");
        }

    }
}
