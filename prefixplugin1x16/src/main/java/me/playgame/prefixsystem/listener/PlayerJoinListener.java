package me.playgame.prefixsystem.listener;

import me.playgame.prefixsystem.PrefixPlugin;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

    private final PrefixPlugin plugin;

    public PlayerJoinListener(PrefixPlugin plugin) {
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void handlePlayerJoinEvent(PlayerJoinEvent event){
        final Player player = event.getPlayer();
        plugin.getPrefixHandler().setPlayerAppearanceAutomatically(player);
    }
}
